package read

import (
	"database/sql"
	"encoding/json"
	"mdb_adodb/config"
	"mdb_adodb/save"
	"os"
	"time"

	"go.uber.org/zap"
	"gopkg.in/guregu/null.v3"
)

var (
	// test
	// DataFilePath   = "C:\\Users\\RETI\\Desktop\\LSIS\\AMOS_DB\\AMOS_DAY_DATA2.mdb"
	// SpecFilePath   = "C:\\Users\\RETI\\Desktop\\LSIS\\XlsData\\HCU_SPEC2.mdb"
	// DriverName     = "adodb"
	// DataSourceName = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\RETI\\Desktop\\LSIS\\AMOS_DB\\AMOS_DAY_DATA2.mdb;Persist Security Info=False"
	// SpecSourceName = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\RETI\\Desktop\\LSIS\\XlsData\\HCU_SPEC2.mdb;Persist Security Info=False"

	// real
	DataFilePath   = "D:\\LSIS\\AMOS_DB\\AMOS_DAY_DATA.mdb"
	SpecFilePath   = "D:\\LSIS\\XlsData\\HCU_SPEC.mdb"
	DriverName     = "adodb"
	DataSourceName = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\\LSIS\\AMOS_DB\\AMOS_DAY_DATA.mdb;Persist Security Info=False"
	SpecSourceName = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\\LSIS\\XlsData\\HCU_SPEC.mdb;Persist Security Info=False"

	finalSaveMdbDatas config.FinalSaveMdbDatas
)

func readMappingData(sugar *zap.SugaredLogger) config.SaveMappingDatasArray {

	var saveMappingDatasArrays config.SaveMappingDatasArray

	a, err := os.ReadFile(config.MappingFile)
	if err != nil {
		sugar.Errorf("[ERROR] file read = %v", err)
	}

	err = json.Unmarshal(a, &saveMappingDatasArrays)
	if err != nil {
		sugar.Errorf("[ERROR] json unmarshal = %v", err)
	}

	b, _ := json.Marshal(saveMappingDatasArrays)
	sugar.Info("saveMappingDatasArrays : ", string(b))

	return saveMappingDatasArrays

}

func readSpecData(sugar *zap.SugaredLogger) config.SpecDataArray {

	var hcuId int
	var dong string
	var houseName string
	var ho string

	var specDataArrays config.SpecDataArray
	var specDatas config.SpecData

	var rows *sql.Rows

	if _, err := os.Stat(SpecFilePath); err != nil {
		sugar.Errorf("[ERROR] database = %v, %s", time.Now(), SpecFilePath)
	}

	db, err := sql.Open(DriverName, SpecSourceName)
	if err != nil {
		sugar.Errorf("[ERROR] spl open = %v", err)
	}
	defer db.Close()

	rows, err = db.Query("SELECT HCU_ID, DONG, HOUSE_NAME, HO FROM TBL_HCU ORDER BY HOUSE_NAME DESC")
	if err != nil {
		sugar.Errorf("[ERROR] db query = %v", err)
	}

	for rows.Next() {
		err := rows.Scan(&hcuId, &dong, &houseName, &ho)
		if err != nil {
			sugar.Errorf("[ERROR] rows scan = %v", err)
		}
		specDatas.HcuId = hcuId
		specDatas.DONG = dong
		specDatas.HouseName = houseName
		specDatas.Ho = ho
		specDatas.Elec = null.FloatFrom(0)
		specDatas.PastElec = null.FloatFrom(0)
		specDataArrays.SpecDatas = append(specDataArrays.SpecDatas, specDatas)
	}
	defer rows.Close()

	b, _ := json.Marshal(specDataArrays)
	sugar.Info("specDataArrays : ", string(b))

	return specDataArrays

}

func CompareMapSpecData(sugar *zap.SugaredLogger) config.CompareDataArray {

	var compareDataArrays config.CompareDataArray
	var compareDatas config.CompareData

	data := make(map[int][]config.CompareData)

	mappingData := readMappingData(sugar)
	specData := readSpecData(sugar)

	for i, v := range mappingData.SaveMappingDatas {
		for _, vv := range specData.SpecDatas {
			if v.Floor == vv.DONG {
				if v.Ho == vv.Ho {
					if v.HcuId == vv.HcuId {
						compareDatas.GroupId = v.GroupId
						compareDatas.HcuId = v.HcuId
						compareDatas.DONG = v.Floor
						compareDatas.Ho = v.Ho
						compareDatas.HouseName = v.HouseName
						compareDatas.Date = vv.Date
						compareDatas.PastDate = vv.PastDate
						compareDatas.Time = vv.Time
						compareDatas.PastTime = vv.PastTime
						compareDatas.Elec = vv.Elec
						compareDatas.PastElec = vv.PastElec
						data[i] = append(data[i], compareDatas)
						compareDataArrays.CompareDatas = append(compareDataArrays.CompareDatas, data[i]...)
					} else {
						// fmt.Println(v.HcuId)
						continue
					}
				} else {
					continue
				}
			} else {
				continue
			}
		}
	}

	// a, _ := json.Marshal(compareDataArrays.CompareDatas)
	// sugar.Infof("CompareDatas : ", string(a))
	// fmt.Println(string(a))

	return compareDataArrays

}

func ReadMdb(sugar *zap.SugaredLogger, compareDataArrays config.CompareDataArray) config.FinalMdbDatas {

	compareDataArray := compareDataArrays.CompareDatas

	var id int
	var elec null.Float
	var date int
	var times int

	var rows *sql.Rows

	if _, err := os.Stat(DataFilePath); err != nil {
		sugar.Errorf("[ERROR] database = %v, %s", time.Now(), DataFilePath)
	}

	db, err := sql.Open(DriverName, DataSourceName)
	if err != nil {
		sugar.Errorf("[ERROR] spl open = %v", err)
	}
	defer db.Close()

	// sugar.Debugf("%v, %s = %v", time.Now(), "db.Ping", db.Ping())
	// sugar.Debugf("%v, %s = %d", time.Now(), "db.InUse", db.Stats().InUse)
	// sugar.Debugf("%v, %s = %d", time.Now(), "db.Idle", db.Stats().Idle)
	// sugar.Debugf("%v, %s = %d", time.Now(), "db.OpenConnections", db.Stats().OpenConnections)

	var finalMdbDatas config.FinalMdbDatas
	data := make(map[int][]config.MdbDatas)

	for i, v := range compareDataArray {
		cnt := 0
		var mdbData config.MdbDatas

		mdbData.HcuId = v.HcuId
		mdbData.Elec = v.Elec
		mdbData.Date = v.Date
		mdbData.Time = v.Time
		mdbData.PastElec = v.PastElec

		rows, err = db.Query("SELECT TOP 2 HCU_ID, ELEC, READ_DATE, READ_TIME FROM TBL_DATA WHERE HCU_ID = `{0}` AND ELEC Not Like 0 ORDER BY READ_DATE DESC, READ_TIME DESC", v.HcuId)
		if err != nil {
			sugar.Errorf("[ERROR] db query = %v", err)
		}
		for rows.Next() {
			err := rows.Scan(&id, &elec, &date, &times)
			if err != nil {
				sugar.Errorf("[ERROR] rows scan = %v", err)
			}
			if cnt == 0 {
				mdbData.Elec = null.FloatFrom(elec.Float64 * 1000)
				mdbData.Date = date
				mdbData.Time = times
				mdbData.HcuId = id
			} else if cnt == 1 {
				mdbData.PastElec = null.FloatFrom(elec.Float64 * 1000)
				mdbData.PastDate = date
				mdbData.PastTime = times
			}
			cnt++
		}
		data[i] = append(data[i], mdbData)
		finalMdbDatas.MdbDatas = append(finalMdbDatas.MdbDatas, data[i]...)
	}
	defer rows.Close()

	// save lastest data each month
	_, err = os.Stat(config.FileName)
	if err != nil {
		sugar.Errorf("[ERROR] file stat = %v", err)
		_, err := os.Create(config.FileName)
		if err != nil {
			sugar.Errorf("[ERROR] file create = %v", err)
		}
		save.SaveData(sugar, finalMdbDatas)
	}

	for _, v := range finalMdbDatas.MdbDatas {
		if v.Date == 0 || v.PastDate == 0 {
			continue
		} else {
			curDate := save.ExtractDate(v.Date, sugar)
			pastDate := save.ExtractDate(v.PastDate, sugar)
			if curDate > pastDate {
				save.SaveData(sugar, finalMdbDatas)
			} else {
				continue
			}
		}
	}

	// fmt.Println(finalMdbDatas)

	// save.SaveData(sugar, finalMdbDatas)

	finalSaveMdbDatas = save.ReadSaveData(sugar)

	finalMdbDatas = save.CompareDate(finalSaveMdbDatas, finalMdbDatas, sugar)

	return finalMdbDatas

}

// func SaveMappingData(sugar *zap.SugaredLogger) {

// 	var saveMappingDatasArray config.SaveMappingDatasArray
// 	var savmeMappingData config.SaveMappingData

// 	var hcuId int
// 	var dong string
// 	var houseName string
// 	var ho string

// 	var rows *sql.Rows

// 	if _, err := os.Stat(SpecFilePath); err != nil {
// 		sugar.Errorf("[ERROR] database = %v, %s", time.Now(), SpecFilePath)
// 	}

// 	db, err := sql.Open(DriverName, SpecSourceName)
// 	if err != nil {
// 		sugar.Errorf("[ERROR] spl open = %v", err)
// 	}
// 	defer db.Close()

// 	rows, err = db.Query("SELECT HCU_ID, DONG, HOUSE_NAME, HO FROM TBL_HCU ORDER BY HOUSE_NAME DESC")
// 	if err != nil {
// 		sugar.Errorf("[ERROR] db query = %v", err)
// 	}

// 	for rows.Next() {
// 		err := rows.Scan(&hcuId, &dong, &houseName, &ho)
// 		if err != nil {
// 			sugar.Errorf("[ERROR] rows scan = %v", err)
// 		}
// 		savmeMappingData.HcuId = hcuId
// 		savmeMappingData.Floor = dong
// 		savmeMappingData.HouseName = houseName
// 		savmeMappingData.Ho = ho
// 		switch dong {
// 		case "B1F":

// 		}
// 		saveMappingDatasArray.SaveMappingDatas = append(saveMappingDatasArray.SaveMappingDatas, savmeMappingData)
// 	}
// 	defer rows.Close()
// }
