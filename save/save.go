package save

import (
	"encoding/json"
	"mdb_adodb/config"
	"os"
	"strconv"
	"strings"

	"go.uber.org/zap"
	"gopkg.in/guregu/null.v3"
)

func SaveData(sugar *zap.SugaredLogger, finalMdbData config.FinalMdbDatas) {

	a, err := json.Marshal(finalMdbData)
	if err != nil {
		sugar.Errorf("[ERROR] json marshal = %v", err)
	}
	err = os.WriteFile(config.FileName, a, 0644)
	if err != nil {
		sugar.Errorf("[ERROR] file write = %v", err)
	}

}

func ExtractDate(fullDate int, sugar *zap.SugaredLogger) int {

	var dateArray []string
	var extDate int

	stringDate := strconv.Itoa(fullDate)
	splitDate := strings.Split(stringDate, "")

	for _, v := range splitDate {
		dateArray = append(dateArray, v)
	}

	yearDay := dateArray[0] + dateArray[1] + dateArray[2] + dateArray[3] + dateArray[4] + dateArray[5]

	extDate, err := strconv.Atoi(yearDay)
	if err != nil {
		sugar.Info(err)
	}

	return extDate
}

func ReadSaveData(sugar *zap.SugaredLogger) config.FinalSaveMdbDatas {
	a, err := os.ReadFile(config.FileName)
	if err != nil {
		sugar.Errorf("[ERROR] file read = %v", err)
	}
	// fmt.Println(string(a))
	var finalSaveMdbDatas config.FinalSaveMdbDatas

	err = json.Unmarshal(a, &finalSaveMdbDatas)
	if err != nil {
		sugar.Errorf("[ERROR] json unmarshal = %v", err)
	}
	// sugar.Info("finalSaveMdbDatas : ", finalSaveMdbDatas)

	return finalSaveMdbDatas
}

func CompareDate(finalSaveMdbDatas config.FinalSaveMdbDatas, finalMdbDatas config.FinalMdbDatas, sugar *zap.SugaredLogger) config.FinalMdbDatas {

	finalSaveMdbData := finalSaveMdbDatas.MdbDatas
	mdbData := finalMdbDatas.MdbDatas

	// sugar.Info("mdbdata", mdbData)

	for _, v := range finalSaveMdbData {
		for i, vv := range mdbData {
			if v.HcuId == vv.HcuId {
				if vv.Date == 0 || v.PastDate == 0 {
					continue
				} else {
					saveDate := ExtractDate(v.PastDate, sugar)
					date := ExtractDate(vv.Date, sugar)
					if saveDate < date {
						mdbData[i].Elec = null.FloatFrom(vv.Elec.Float64 - v.PastElec.Float64)
					} else {
						continue
					}
				}
			} else {
				continue
			}
		}
	}
	// a, _ := json.Marshal(finalMdbDatas)
	// fmt.Println("asdf : ", string(a))

	return finalMdbDatas
}
