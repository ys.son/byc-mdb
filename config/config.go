package config

import (
	"encoding/json"
	"os"
	"time"

	"go.uber.org/zap"
	"gopkg.in/guregu/null.v3"
)

var (
	Format        = time.Now().Format("2006-01-02 15:04:05")
	sugar         *zap.SugaredLogger
	EnvConfigPath = "./config.json"

	// test
	// FileName = "C:\\Users\\RETI\\Desktop\\workspaces\\mdb_odbc\\saveData.json"
	// MappingFile   = "./mappingData1.json"

	// real
	FileName    = "C:\\Users\\user\\Desktop\\reti\\saveData.json"
	MappingFile = "./mappingData.json"
)

// 설정 정보
type Configurations struct {
	Dev Configuration `json:"dev"`
	Pro Configuration `json:"prod"`
}

type Configuration struct {
	// Servers []Server   `json:"servers"`
	Period int        `json:"period"`
	Cfg    zap.Config `json:"logging"`
}

// mpping data json array
type SaveMappingDatasArray struct {
	SaveMappingDatas []SaveMappingData
}

type SaveMappingData struct {
	Floor     string
	Ho        string
	GroupId   int
	HcuId     int
	HouseName string
}

type FinalSaveMdbDatas struct {
	MdbDatas []SaveMdbData
}

type SaveMdbData struct {
	HcuId    int        `json:"HcuId"`
	Elec     null.Float `json:"Elec"`
	Date     int        `json:"Date"`
	Time     int        `json:"Time"`
	PastElec null.Float `json:"PastElec"`
	PastDate int        `json:"PastDate"`
	PastTime int        `json:"PastTime"`
}

// spec data
type SpecDataArray struct {
	SpecDatas []SpecData
}

type SpecData struct {
	HcuId     int
	DONG      string
	Ho        string
	HouseName string
	Elec      null.Float
	Date      int
	Time      int
	PastElec  null.Float
	PastDate  int
	PastTime  int
}

type CompareDataArray struct {
	CompareDatas []CompareData
}

type CompareData struct {
	GroupId   int
	HcuId     int
	DONG      string
	Ho        string
	HouseName string
	Elec      null.Float
	Date      int
	Time      int
	PastElec  null.Float
	PastDate  int
	PastTime  int
}

// mdb Data
type FinalMdbDatas struct {
	MdbDatas []MdbDatas
}

type MdbDatas struct {
	HcuId    int
	Elec     null.Float
	Date     int
	Time     int
	PastElec null.Float
	PastDate int
	PastTime int
}

// server로 보낼 data
type MergedData struct {
	ProtocolVersion string      `json:"protocolVersion,omitempty"` //장비 데이터의 contents의 protocol version. 현재는 사용되지 않음
	Timestamp       interface{} `json:"timestamp"`
	MissingData     bool        `json:"missingData"`
	Serial          string      `json:"serial"`
	GroupData       []GroupData `json:"groupData"`
}

type GroupData struct {
	Id   int    `json:"id"`
	Data []Data `json:"data"`
	// ModuleData []ModuleData `json:"moduleData,omitempty"`
}

// type ModuleData struct {
// 	Id   int    `json:"id"`
// 	Data []Data `json:"data"`
// }

type Data struct {
	Name  string     `json:"name"`
	Value null.Float `json:"value"`
}

// LoadConfig : config.json에 있는 설정 정보 불러오기
func LoadConfig(configPath string) (*zap.SugaredLogger, *zap.Logger) {

	conf := loadConfiguration(configPath)

	logger, err := conf.Cfg.Build()
	if err != nil {
		sugar.Errorf("%v : [ERROR] sugar build failed = %v", Format, err)
	}
	sugar = logger.Sugar()

	return sugar, logger

}

// LoadConfiguration : config.json에 있는 설정 정보 불러오기
func loadConfiguration(configPath string) Configuration {

	var config Configuration

	configFile, err := os.Open(configPath)
	if err != nil {
		sugar.Errorf("%v : [ERROR] config file open failed = %v", Format, err)
	}
	defer configFile.Close()

	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		sugar.Errorf("%v : [ERROR] decode config file failed = %v", Format, err)
	}

	return config

}

// type Server struct {
// 	Ip            string `json:"ip"`
// 	Port          string `json:"port"`
// 	Method        string `json:"method"`
// 	Period        int    `json:"period"`
// 	Version       string `json:"version"`
// 	Type          string `json:"type"`
// 	Path          string `json:"path"`
// 	DataAccessKey string `json:"dataAccessKey"`
// 	DBAccess      bool   `json:"db_access"` //장비의 누적 데이터를 서버와 동기화. with primary

// 	DisableTruncate bool  `json:"disableTruncate"` //DisableTruncate, TimeShift 는 함께 설정. primary 서버에 전송시 예외적으로 초를 보존하고, TimeShift(ms)만큼 시간을 조정(전송시간 delay 조정을 목적으로 과제에서 사용했음)
// 	TimeShift       int64 `json:"timeShift"`

// 	Alarm                    Alarm  `json:"alarm"`
// 	ProtocolVersion          string `json:"protocolVersion"`
// 	Recovery                 bool   `json:"recovery,omitempty"`
// 	ConfigurationSentSuccess bool
// }

// type Alarm struct {
// 	Enable bool      `json:"enable"`
// 	Queue  QueueInfo `json:"queue"` //하나의 조건을 만족해도 queue에서 지워짐
// }

// type QueueInfo struct {
// 	Period int `json:"period"` //최대보존할 시간 ms
// 	Count  int `json:"count"`  //최대보존할 개수
// }

// type ModbusConfig struct {
// 	RTU       RTUConfig `json:"rtu"`
// 	TCP       TCPConfig `json:"tcp"`
// 	ModbusMap string    `json:"map" default:"4.1.0"`
// 	FixedId   bool      `json:"fixedId" default:"true"`
// }

// type RTUConfig struct {
// 	Enable   bool   `json:"enable"`
// 	Id       int    `json:"id"`
// 	Serial   string `json:"serial" default:"RS232"`
// 	Port     string `json:"port" default:"/dev/ttyUSB0"`
// 	BaudRate int    `json:"baudRate" default:"9600"`
// 	DataBits int    `json:"dataBits" default:"8"`
// 	StopBits int    `json:"stopBits" default:"1"`
// 	Parity   string `json:"parity" default:"N"`
// }

// type TCPConfig struct {
// 	Enable bool `json:"enable"`
// 	Port   int  `json:"port"`
// 	ID     int  `json:"id"`
// }

// sort data
// type SortDatasArray struct {
// 	ASortDatas   ASortDatas
// 	BSortDatas   BSortDatas
// 	CSortDatas   CSortDatas
// 	F1SortDatas  F1SortDatas
// 	F2SortDatas  F2SortDatas
// 	B1FSortDatas B1FSortDatas
// 	B2FSortDatas B2FSortDatas
// 	BiSortDatas  BiSortDatas
// }

// type ASortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type BSortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type CSortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type F1SortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type F2SortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type B1FSortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type B2FSortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }
// type BiSortDatas struct {
// 	Serial    string
// 	AccessKey string
// 	SortDatas []SortData
// }

// type SortData struct {
// 	GroupId   int
// 	HcuId     int
// 	HouseName string
// }

// InitConfig : 설정정보 초기화
// func InitConfig(mode string) error {

// 	var configurations Configurations
// 	if err := loadConfig(EnvConfigPath, &configurations); err != nil {
// 		return fmt.Errorf("%s : %v", "load config failed", err)
// 	}

// 	switch mode {
// 	case "prod":
// 		fmt.Println("init config prod mode")
// 		ModeConfig = configurations.Pro

// 	case "dev":
// 		fmt.Println("init config dev mode")
// 		ModeConfig = configurations.Dev

// 	default:
// 		fmt.Println("init config prod mode")
// 		ModeConfig = configurations.Pro

// 	}
// 	return nil

// }

// InitConfig : 설정정보 초기화
// func loadConfig(configPath string, configData *Configurations) error {

// 	configFile, err := os.Open(configPath)
// 	if err != nil {
// 		return fmt.Errorf("%s, %v", "open config path failed", err)
// 	}
// 	defer configFile.Close()

// 	jsonParser := json.NewDecoder(configFile)
// 	err = jsonParser.Decode(&configData)
// 	if err != nil {
// 		return fmt.Errorf("%s, %v", "json parser config path failed", err)
// 	}

// 	return nil

// }
