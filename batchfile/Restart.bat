@Echo off
CD C:\Users\RETI\Desktop\workspaces\mdb_odbc\
TASKLIST | FIND "mdb_odbc.exe" > NUL
IF NOT ERRORLEVEL 1 (
	ECHO Running
	GOTO PGOK
) ELSE (
	ECHO Not Running
	GOTO PGNG
)

 :PGOK
	ECHO Nothing
	GOTO END
 :PGNG
	ECHO Restart. %DATE% %TIME% >> process.txt
	START C:\Users\RETI\Desktop\workspaces\mdb_odbc\mdb_odbc.exe
	GOTO END
 :END
	ECHO
EXIT