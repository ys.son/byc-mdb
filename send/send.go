package send

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"mdb_adodb/config"
	"net/http"
	"strconv"
	"strings"
	"time"

	"go.uber.org/zap"
	"gopkg.in/guregu/null.v3"
)

const (
	NOERROR = "NOERROR"
	ERROR   = "ERROR"
)

// timeToStamp : mdb에서 읽은 time을 json value 형태로 변환
func timeToStamp(readDate, readTime int) time.Time {

	var dateArray []string
	var timeArray []string
	var year string
	var month string
	var day string
	var hour string
	var min string
	var sec string

	stringDate := strconv.Itoa(readDate)
	splitDate := strings.Split(stringDate, "")

	for _, v := range splitDate {
		dateArray = append(dateArray, v)
	}
	year = dateArray[0] + dateArray[1] + dateArray[2] + dateArray[3]
	if dateArray[4] == "0" {
		month = dateArray[5]
	} else {
		month = dateArray[4] + dateArray[5]
	}
	day = dateArray[6] + dateArray[7]

	intYear, _ := strconv.Atoi(year)
	intMonth, _ := strconv.Atoi(month)
	intDay, _ := strconv.Atoi(day)

	stringTime := strconv.Itoa(readTime)
	splitTime := strings.Split(stringTime, "")

	for _, v := range splitTime {
		timeArray = append(timeArray, v)
	}
	hour = timeArray[0] + timeArray[1]
	min = timeArray[2] + timeArray[3]
	sec = timeArray[4] + timeArray[5]

	intHour, _ := strconv.Atoi(hour)
	intMin, _ := strconv.Atoi(min)
	intSec, _ := strconv.Atoi(sec)

	mdbDate := time.Date(intYear, time.Month(intMonth), intDay, intHour, intMin, intSec, 0, time.Local)

	return mdbDate

}

// unix time으로 변환
func UnixTime(readDate, readTime int) int64 {
	if readDate == 0 {
		return 0
	}
	recentDate := timeToStamp(readDate, readTime)
	// pastDates := timeToStamp(pastDate, pastTime)

	unixRecentDate := recentDate.Unix()
	// unixPastDate := pastDates.Unix()

	return unixRecentDate

}

// checkResponseData : server로부터 온 response를 check
func checkResponseData(checkResult bool, checkErrorMessage bool, responseBody string) ([]string, error) {

	warnings := []string{}

	responseBodyMap := make(map[string]interface{})

	if err := json.Unmarshal([]byte(responseBody), &responseBodyMap); err != nil {
		return warnings, errors.New(fmt.Sprintf("The response cannot be interpreted. responseBody(%v)", string(responseBody)))
	}

	if checkResult {
		if val, ok := responseBodyMap["result"]; ok {
			switch val.(type) {
			case string:
				if val != NOERROR {
					return warnings, errors.New("The result value is not NOERROR")
				}
			default:
				return warnings, errors.New("The result value is not a string.")
			}
		} else {
			return warnings, errors.New("The result item is not included.")
		}
	}

	if checkErrorMessage {
		if val, ok := responseBodyMap["errorMessage"]; ok {
			switch val.(type) {
			case string:
			default:
				warnings = append(warnings, "The errorMessage value is not a string.")
			}
		} else {
			warnings = append(warnings, "The errorMessage item is not included.")
		}
	}

	for key, _ := range responseBodyMap {
		if key == "result" && !checkResult {
			warnings = append(warnings, "The result item is not needed, but has been added.")
		} else if key == "errorMessage" && !checkErrorMessage {
			warnings = append(warnings, "The errorMessage item is not needed, but has been added.")
		} else {
			if (key == "result") || (key == "errorMessage") {
				continue
			}

			warnings = append(warnings, fmt.Sprintf("Contains %v items that are not permitted.", key))
		}
	}

	return warnings, nil

}

// randFloats : random 값
func randFloats(min, max float64, n int) []float64 {

	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res

}

// SendData : mdb에서 읽은 data를 서버로 전송하는 함수
func SendData(sugar *zap.SugaredLogger, compareDataArray config.CompareDataArray, finalMdbDatas config.FinalMdbDatas) {

	curTime := time.Now()

	var dataAccessKey string
	var serial string
	var cnt int

	floorArray := []string{"B2F", "B1F", "1F", "2F", "A", "B", "C", "bi"}
	// floorArray := []string{"B1F"}

	for _, floor := range floorArray {
		switch floor {
		case "B1F":
			// test
			// dataAccessKey = "xrrLkMjTDtGblDCHWRqw13R9j90ylm3wokKpmllBJSUqKaQPe/+yVMGBeXEeG3GvsPoLgnG0gEssHuuVPvC5JF2YutF0+lWQJboaKdn9EXjK4EjjmX6z1CLDIcxT33A5+JINTAVFc01/UCiXNHfpMdzKcfk5mzpuZcaYqw5HkMM5II6B"
			// serial = "AAFFFFFFFFFF"

			// real
			dataAccessKey = "NRRue8EU3uGQ7kHQJer4OXG2XJxF2KegSlGavupqRX/DVsk5lb2vpY9SwUr2o2iKPnrz34lfmoeQ/pEy0smPI4wZAcfzvdBOwZPhAcynkTMOH4raBfNRBBjCM2aJLEdmlDKQDBTt7r7xILXtEkJsns7R7pGub7IKnwSt/p2JBKjgvETU"
			serial = "B1FFFFFFFFFF"
			cnt = 32
		case "B2F":
			dataAccessKey = "z0IzkKh2Q86DToUOY1FphJwsCdooH/VyrtKTwfUq8oRRriuL05AbA5mGizPI6SE1ylCuAlOUZPDs1QjxKMpSHyTTdpEI2hi4YX33x2jv9SDmIh7TyfGKUxQuIuKnTNmZAR20WA4Z8I6HwTXnASwF7Bpv9ehUaEMqYryl/iK5Sc8wC7k4"
			serial = "B2FFFFFFFFFF"
			cnt = 9
		case "1F":
			dataAccessKey = "cPe+pE4E3f3IJ1lM6QPJkbQsnMtylv+Hxn1WyTlRuFBo60PNTE1qKo3D2IM10joGlWeMutvTKImKNy8ZiByas1DJSxO93dfHQ5jznSHBt/sjdA1FSud5IfODAu5LjJObZjgPAqvftDwbrYaxLJGOJ1ClqibOZk/WDOQyiGC/ZCBFGzxm"
			serial = "1FFFFFFFFFFF"
			cnt = 28
		case "2F":
			dataAccessKey = "4riIvkn/6vL2NMLxMnNhfCch7Q7eNc8nQ+AWXyUIsbkArgcH1fgh2h4LvtM/lUGjveB/wzJbXTjmtVrMAlqDaV1pIb6b5Xb0p5taMFEIcoonpQxzulJWn/rkvUFXRSt2NURe2wEzOYvYyamL73j6GI5j6FEY+jpeQJtWH+XKaQTiwVQk"
			serial = "2FFFFFFFFFFF"
			cnt = 26
		case "A":
			dataAccessKey = "gTC18sDOP8ByHoYdmhpflCdTTbr/EagFRq7NG0QC2TZZm+BIPuxHZPv+TeAtUP1/uPvBKru7vmTh8g6Mo5JVotL1vluFe5+KwSdET5UM9j27QuYFfE4XIIxDL++YJ0O1ZydjehYhDUwI8xj96MNiUZqb6HyC/95x5IwaBQKRkruvpcVo"
			serial = "AFFFFFFFFFFF"
			cnt = 155
		case "B":
			dataAccessKey = "M8S+hr7OX2exUodqgE0JdutIQQ4DIOQDPyRSCuONyPCuMrVh+7tWvg80JTBpXpDusgXzAnSxB+QnUg3zbPfW6ZyvNfLufqb6XEK20QmkFJtzrGdqU9cX4yjDNC8rGN/QeMjsjbzlNxir1gQomwxw+fFsB1deUY2kYo7+gkaIbihZ/QU0"
			serial = "BFFFFFFFFFFF"
			cnt = 145
		case "C":
			dataAccessKey = "oRQMPyAPdOL9cSn3U99TZdfIfrQxTkQqUdgMf9fb38CJt15A8940ENcynb4XU3KLG3aJ9/3WnFLk/iFoPEzuXmvrQzzr9JuHtvD/nXibUcbd+BT73C0s9woj+DFeNp4Sp96VFsblXTbCCmsbSqPfXwENWUjMwMDtpjCyrjZsymVqSE5z"
			serial = "CFFFFFFFFFFF"
			cnt = 92
		case "bi":
			dataAccessKey = "mW+lB5Jgyg0kW1ba6VduRXm95uqNt9XBkL76xNV3nsZ4B5QRni/wR3Bk99bAIvlAizstYYZYhR4z0JM/KboY1kO0bggiwnjhvUkeP05xAu91PMBzOCUCi5Rk1ys7o0LKaUooT8BMYWJNn0fZKb0yEfEYQd0ltHvYidWlSWGpGfHr5t0J"
			serial = "DFFFFFFFFFFF"
			cnt = 4
		default:
			continue
		}
		mergeData := dataToJson(compareDataArray, finalMdbDatas, floor, serial, cnt, curTime)
		err := SendMergeData(mergeData, dataAccessKey)
		if err != nil {
			sugar.Errorf("[ERROR] send merge data = %v", err)
		}
	}
}

// dataToJson : mdb에서 읽은 data를 server로 보낼 수 있게 json화
func dataToJson(compareDataArray config.CompareDataArray, finalMdbDatas config.FinalMdbDatas, floor, serial string, cnt int, curTime time.Time) config.MergedData {

	// 가상 Data를 생성하기 위한 Unix time과 누적전력량
	// curUnix := UnixTime(vv.Date, vv.Time)
	// pastUnix := UnixTime(vv.PastDate, vv.PastTime)
	// minUnix := (curUnix - pastUnix) / 60

	// divElec := vv.Elec.Float64 - vv.PastElec.Float64
	// minElec := divElec / float64(minUnix)

	compareDatas := compareDataArray.CompareDatas
	mdbDatas := finalMdbDatas.MdbDatas

	var merge config.MergedData
	var groupData config.GroupData

	var data config.Data
	datas := make(map[int][]config.Data)

	var virElec float64

	// fmt.Println(compareDatas)

	for i, v := range compareDatas {
		if v.DONG == floor {
			merge.Serial = serial
			for _, vv := range mdbDatas {
				if v.HcuId == vv.HcuId {
					if v.GroupId >= cnt {
						break
					} else {
						randLineVol := randFloats(375, 385, 1)
						randPf := randFloats(0.89, 0.95, 1)
						randFreq := randFloats(59, 60, 1)
						randTheta := math.Acos(randPf[0])

						virElec = vv.Elec.Float64 - vv.PastElec.Float64
						if virElec < 0 {
							virElec = 0
						}

						randPhaVol := randLineVol[0] / math.Cbrt(3.0)
						randPhaCur := virElec / (randPhaVol * 3 * randPf[0])
						randApparent := 3 * randPhaVol * randPhaCur
						randActPower := randApparent * randPf[0]
						randReactPower := randApparent * math.Sin(randTheta)

						if vv.Elec == null.FloatFrom(0) {
							for j := 0; j < 9; j++ {
								if j == 0 {
									data.Name = "ACCUMULATE_POWER_CONSUMPTION"
									data.Value = vv.Elec
								} else if j == 1 {
									data.Name = "AVERAGE_PHASE_VOLTAGE"
									data.Value = null.FloatFrom(0)
								} else if j == 2 {
									data.Name = "AVERAGE_LINE_VOLTAGE"
									data.Value = null.FloatFrom(0)
								} else if j == 3 {
									data.Name = "AVERAGE_PHASE_CURRENT"
									data.Value = null.FloatFrom(0)
								} else if j == 4 {
									data.Name = "POWER_FACTOR"
									data.Value = null.FloatFrom(0)
								} else if j == 5 {
									data.Name = "AVERAGE_FREQUENCY"
									data.Value = null.FloatFrom(0)
								} else if j == 6 {
									data.Name = "SUM_ACTIVE_POWER"
									data.Value = null.FloatFrom(0)
								} else if j == 7 {
									data.Name = "SUM_REACTIVE_POWER"
									data.Value = null.FloatFrom(0)
								} else if j == 8 {
									data.Name = "SUM_APPARENT_POWER"
									data.Value = null.FloatFrom(0)
								}
								datas[i] = append(datas[i], data)
							}
						} else {
							for j := 0; j < 9; j++ {
								if j == 0 {
									data.Name = "ACCUMULATE_POWER_CONSUMPTION"
									data.Value = vv.Elec
								} else if j == 1 {
									data.Name = "AVERAGE_PHASE_VOLTAGE"
									data.Value = null.FloatFrom(randPhaVol)
								} else if j == 2 {
									data.Name = "AVERAGE_LINE_VOLTAGE"
									data.Value = null.FloatFrom(randLineVol[0])
								} else if j == 3 {
									data.Name = "AVERAGE_PHASE_CURRENT"
									data.Value = null.FloatFrom(randPhaCur)
								} else if j == 4 {
									data.Name = "POWER_FACTOR"
									data.Value = null.FloatFrom(randPf[0])
								} else if j == 5 {
									data.Name = "AVERAGE_FREQUENCY"
									data.Value = null.FloatFrom(randFreq[0])
								} else if j == 6 {
									data.Name = "SUM_ACTIVE_POWER"
									data.Value = null.FloatFrom(randActPower)
								} else if j == 7 {
									data.Name = "SUM_REACTIVE_POWER"
									data.Value = null.FloatFrom(randReactPower)
								} else if j == 8 {
									data.Name = "SUM_APPARENT_POWER"
									data.Value = null.FloatFrom(randApparent)
								}
								datas[i] = append(datas[i], data)
							}
						}
						groupData.Data = datas[i]
						groupData.Id = v.GroupId
						merge.GroupData = append(merge.GroupData, groupData)
					}
				} else {
					continue
				}
			}
		} else {
			continue
		}
	}
	merge.Timestamp = curTime

	return merge
}

//SendMergeData : json data를 서버로 전송
func SendMergeData(merge config.MergedData, dataAccessKey string) error {

	b := new(bytes.Buffer)

	json.NewEncoder(b).Encode(merge)

	url := "https://dev.energywatch.kr:443/api/v1/metering-devices/data"
	method := "https"
	maxTryCount := 5
	host := "dev.energywatch.kr"

	var client *http.Client
	var request *http.Request
	var err error
	var responseLastError error
	var responseLastWarnings []string

	if request, err = http.NewRequest("POST", url, bytes.NewReader([]byte(b.String()))); err != nil {
		return err
	}

	if dataAccessKey != "" {
		request.Header.Add("Data-Access-Key", dataAccessKey)
	}

	request.Header.Set("Content-Type", "application/json; charset=utf-8")

	for tryCount := 0; tryCount < maxTryCount; tryCount++ {

		if method == "http" {
			client = &http.Client{
				Timeout: time.Duration(10 * time.Second),
			}
		} else {
			tr := &http.Transport{
				//TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			}

			client = &http.Client{
				Transport: tr,
				Timeout:   time.Duration(10 * time.Second),
			}
		}

		var response *http.Response
		// log.Printf("[%s] sendMergeData Sending merged data time(%v) tryCount(%v)", url, merge.Timestamp, tryCount+1)
		log.Printf("sendMergeData [%s]", b.String())
		if response, err = client.Do(request); err != nil {
			// responseLastError = err
			continue
		}

		defer response.Body.Close()

		body, _ := ioutil.ReadAll(response.Body)

		responseLastWarnings, responseLastError = checkResponseData(true, true, string(body))

		log.Printf("[%s] Received sendMergeData response code(%v) data(%s) responseWarnings(%v)", host, response.StatusCode, string(body), responseLastWarnings)

		if responseLastError == nil {
			break
		}
		return err
	}

	return nil
}
