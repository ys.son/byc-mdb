package send

type TotalData struct {
	ForceUpdate       bool              `json:"forceUpdate"`
	SystemInfos       SystemInfo        `json:"systemInfo"`
	DistributionBoard DistributionBoard `json:"distributionBoard"`
}

type SystemInfo struct {
	Meters         Meter         `json:"meter"`
	ComponentInfos ComponentInfo `json:"componentInfo"`
	ControlBoards  ControlBoard  `json:"controlBoard"`
	Module         Module        `json:"module"`
	timeZone       string        `json:"timeZone"`
}

type Meter struct {
	Model   string `json:"model"`
	Serial  string `json:"serial"`
	Version string `json:"version"`
}

type ComponentInfo struct {
	Group_ranges []int  `json:"group_range"`
	Name         string `json:"name"`
	Type         string `json:"type"`
	Version      string `json:"version"`
}

//type Group_range struct {
//	GroupID int
//}

type ControlBoard struct {
	version string `json:"version"`
}

type Module struct {
	version string `json:"version"`
}

type DistributionBoard struct {
	AlarmItems []AlarmItem `json:"alarmItems"`
	Groups     []Group     `json:"groups"`
}

type AlarmItem struct {
	Item int
}

type Group struct {
	Id           int           `json:"id"`
	Name         string        `json:"name"`
	Type         string        `json:"type"`
	ElecType     string        `json:"elecType"`
	GroupModules []GroupModule `json:"modules"`
}

type GroupModule struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Phase    string `json:"phase"`
	Rate     string `json:"rate"`
	ElecType string `json:"elecType"`
	CtType   string `json:"ctType"`
}

// func SendConfig(sortDatasArray config.SortDatasArray, specDataArray config.SpecDataArray) {

// 	var totalData TotalData
// 	var systemInfo SystemInfo
// 	var distributionBoard DistributionBoard
// 	var meter Meter
// 	var componentInfo ComponentInfo
// 	//var group_range Group_range
// 	var controlBoard ControlBoard
// 	var module Module
// 	//var alarmItem AlarmItem
// 	var group Group
// 	var groupModule GroupModule

// 	groupModuleData := make(map[int][]GroupModule)
// 	//groupData := make(map[int][]Group)

// 	totalData.ForceUpdate = false
// 	systemInfo.timeZone = "Asia/Seoul"
// 	meter.Model = "CBN"
// 	meter.Version = "1.0.0"

// 	componentInfo.Name = "ds_125"
// 	componentInfo.Type = "ds_engine"
// 	componentInfo.Version = "3.1.10"
// 	meter.Serial = "B1FFFFFFFFFF"

// 	for i, v := range sortDatasArray.ASortDatas.SortDatas {
// 		group.Id = v.GroupId
// 		group.Name = v.HouseName
// 		group.Type = "C3P3W"
// 		group.ElecType = "AC"
// 		//group_range.GroupID = i + 1
// 		componentInfo.Group_ranges = append(componentInfo.Group_ranges, i+1)
// 		for j := 0; j < 3; j++ {
// 			groupModule.Id = j + 1
// 			groupModule.Rate = "_600A_"
// 			groupModule.ElecType = "AC"
// 			groupModule.CtType = "None"
// 			if j == 0 {
// 				groupModule.Name = "Device#1"
// 				groupModule.Phase = "R"
// 			} else if j == 1 {
// 				groupModule.Name = "Device#2"
// 				groupModule.Phase = "S"
// 			} else if j == 2 {
// 				groupModule.Name = "Device#1"
// 				groupModule.Phase = "T"
// 			}
// 			groupModuleData[i] = append(groupModuleData[i], groupModule)
// 		}
// 		group.GroupModules = groupModuleData[i]
// 		distributionBoard.Groups = append(distributionBoard.Groups, group)
// 	}
// 	systemInfo.Meters = meter
// 	systemInfo.ComponentInfos = componentInfo
// 	controlBoard.version = "1.0.0"
// 	systemInfo.ControlBoards = controlBoard
// 	//systemInfo.ControlBoards = append(systemInfo.ControlBoards, controlBoard)
// 	module.version = "1.0.0"
// 	systemInfo.Module = module
// 	//systemInfo.Module = append(systemInfo.Module, module)
// 	totalData.SystemInfos = systemInfo
// 	totalData.DistributionBoard = distributionBoard

// 	a, _ := json.Marshal(totalData)
// 	fmt.Println(string(a))
// }
