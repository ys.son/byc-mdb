package main

import (
	"mdb_adodb/config"
	"mdb_adodb/read"
	"mdb_adodb/send"
	"time"

	_ "github.com/mattn/go-adodb"
)

var (
	// version : 프로그램 버전
	version = "virtual_verison.1"
	// sortDatasArray config.SortDatasArray
	compareDataArray config.CompareDataArray
	finalMdbData     config.FinalMdbDatas
)

func main() {

	// 설정 정보 불러오기
	sugar, logger := config.LoadConfig(config.EnvConfigPath)
	defer logger.Sync() // flushes buffer, if any

	sugar.Infof("%v : %s", config.Format, " MDB_ODBC "+version+" for BEMS ")

	// amos spec mdb read
	compareDataArray = read.CompareMapSpecData(sugar)
	finalMdbData = read.ReadMdb(sugar, compareDataArray)

	// 누적전력량 data를 가진 db read
	send.SendData(sugar, compareDataArray, finalMdbData)

	// go func을 무한 실행하기 위한 channel
	loopChan := make(chan interface{})

	go func() {
		minTicker := time.NewTicker(5 * time.Minute)
		for {
			select {
			case <-minTicker.C:
				finalMdbData = read.ReadMdb(sugar, compareDataArray)
			default:
				continue
			}
		}
	}()

	go func() {
		minTicker := time.NewTicker(1 * time.Minute)
		for {
			select {
			case <-minTicker.C:
				send.SendData(sugar, compareDataArray, finalMdbData)
			default:
				continue
			}
		}
	}()

	<-loopChan
}
