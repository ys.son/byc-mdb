module mdb_adodb

go 1.17

require (
	github.com/mattn/go-adodb v0.0.1
	gopkg.in/guregu/null.v3 v3.5.0
)

require (
	github.com/go-ole/go-ole v1.2.5 // indirect
	go.uber.org/zap v1.19.1
	golang.org/x/net v0.0.0-20210917221730-978cfadd31cf // indirect
	golang.org/x/sys v0.0.0-20210921065528-437939a70204 // indirect

)

require (
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
)

require github.com/jinzhu/inflection v1.0.0 // indirect
