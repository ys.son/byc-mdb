//package recovery는 ds_aggregator의 네트워크 장애시 제전송 기능(복구)이 구현되어 있다.
/*
Description

복구 기능은 데이터 전송 실패를 기록 후 전송이 성공하게 되면 실패 시점부터 성공시점까지 데이터를 DB(postgresql)으로부터 읽어 제전송하는 기능이다.

TODO:
  - 한번에 데이터를 여러 시간을 보내도록 기능이 추가가 핗요
    (프로토콜 변경이 필요함)
  - 복구 파일이 깨졌을 때 대응코드 필요
*/

package recovery

// import (
// 	"bytes"
// 	"encoding/json"
// 	"errors"
// 	"fmt"
// 	"io/ioutil"
// 	"net/http"
// 	"sync"
// 	"time"

// 	"gopkg.in/guregu/null.v3"
// )

// type RecoveryInfoArray struct {
// 	recoveryInfos []RecoveryInfo
// }

// type RecoveryInfo struct {
// 	AccessKey string
// 	Serial    string
// 	Time      time.Time
// 	HcuId     int
// }

// type FinalRecoveryMdbData struct {
// 	recoveryMdbData []RecoveryMdbData
// }

// type RecoveryMdbData struct {
// 	HcuId int
// 	Elec  null.Float
// 	Date  int
// 	Time  int
// }

// var (
// 	GlobalMtx = sync.Mutex{}
// 	FilePath  = "./.recovery.json"
// )

// type MergeDataWithTime struct {
// 	StartTime time.Time
// 	Data      config.MergedData
// }

// type RecoveryElement struct {
// 	Host          string            `json:"host,omitempty"`
// 	Port          uint16            `json:"port,omitempty"`
// 	StartTime     time.Time         `json:"start_time,omitempty"`
// 	FirstFailData config.MergedData `json:"first_data,"omitempty`
// }

// type Recovery struct {
// 	ConfigPath     string
// 	DBConnect      *gorm.DB
// 	NetworkFailure *sync.Map //네트워크 장애 정보를 담고있는 map. key는 host 정보, value는 RecoveryElement
// 	Logger         *zap.SugaredLogger
// 	Running        *sync.Map //복구가 실행중인지를 판단. key는 host 정보, value는 true/false
// }

// type GroupMap struct {
// 	GroupID  int
// 	ModuleID []int
// }

// type ConfigurationInfo struct {
// 	Key           string      `json:"key"`
// 	ComponentInfo interface{} `json:"componentInfo"`
// 	SensorInfo    SensorInfo  `json:"sensorInfo"`
// 	Resource      Resoruce    `json:"resource"`
// }

// type SensorInfo struct {
// 	Groups     []Group  `json:"groups"`
// 	AlarmItems []string `json:"alarmItems,omitempty"`
// }

// type Resoruce struct {
// 	Period int `json:"period"`
// 	//GroupRange  Range `json:"groupRange"`
// 	//ModuleRange Range `json:"moduleRange"`

// 	GroupRange  []int `json:"groupRange"`
// 	ModuleRange []int `json:"moduleRange"`
// }

// type Group struct {
// 	//common fields
// 	Id   int    `json:"id"`
// 	Name string `json:"name"`
// 	//for ds_engine
// 	Type       string   `json:"type"`
// 	ElecType   string   `json:"elecType"`
// 	DataItems  []string `json:"dataItems,omitempty"`
// 	AlarmItems []string `json:"alarmItems,omitempty"`
// 	Modules    []Module `json:"modules"`
// }

// type Module struct {
// 	//common fields
// 	Id   int    `json:"id"`
// 	Name string `json:"name"`
// 	//for ds_engine
// 	Phase      string   `json:"phase"`
// 	Rate       string   `json:"rate"`
// 	ElecType   string   `json:"elecType"`
// 	SensorType string   `json:"sensorType,omitempty"`
// 	DataItems  []string `json:"dataItems,omitempty"`
// 	AlarmItems []string `json:"alarmItems,omitempty"`
// 	CTType     string   `json:"ctType,omitempty"`
// }

// type Serial struct {
// 	Serials []string
// }

// func RecoveryCheck(sortDatasArray config.SortDatasArray, finalMdbDatas config.FinalMdbDatas) {

// 	var s config.Server

// 	port, _ := strconv.Atoi(s.Port)
// 	var dataSender = make(chan MergeDataWithTime)

// 	for

// 	// serial이 여러개
// 	go recoveryObj.StartFailoverSender(dataSender, s.Recovery, configMap, s.DataAccessKey, serial, s.Method, s.Ip, uint16(port), s.Version, s.Path, s.ProtocolVersion, "last")

// 	ticker := time.NewTicker(time.Duration(s.Period) * time.Millisecond)
// 	quit := make(chan struct{})

// 	for {
// 		select {
// 		case <-ticker.C:

// 			var mergedData config.MergedData
// 			mergedData.Serial = config.Meter.Serial
// 			mergedData.MissingData = false

// 			if len(s.ProtocolVersion) != 0 {
// 				mergedData.ProtocolVersion = s.ProtocolVersion
// 			}

// 			nowTime := time.Now()

// 			if len(s.ProtocolVersion) == 0 {
// 				if len(s.Version) == 0 {
// 					mergedData.Timestamp = fmt.Sprintf("\\/Date(%d000)/\\", int64(nowTime.Unix())+(s.TimeShift/1000))
// 				} else {
// 					mergedData.Timestamp = nowTime.Add(time.Duration(s.TimeShift) * time.Millisecond).Format(time.RFC3339Nano)
// 				}
// 			} else {
// 				mergedData.Timestamp = nowTime.Add(time.Duration(s.TimeShift) * time.Millisecond).Format(time.RFC3339Nano)
// 			}

// 			for _, v := range dataMap {
// 				mergedData.GroupData = append(mergedData.GroupData, (*v).GroupData...)
// 			}

// 			if len(mergedData.GroupData) == 0 {
// 				sugar.Infof("[%s] mergedData.GroupData array is zero", s.Ip)
// 				continue
// 			}

// 			dataSender <- recovery.MergeDataWithTime{
// 				StartTime: time.Now(),
// 				Data:      mergedData,
// 			}
// 		case <-quit:
// 			ticker.Stop()
// 			return
// 		}
// 	}
// }

// //saveRecovery 함수는 복구 관련 정보를 저장하는 기능을 한다. 복구 시작시간을 기록한다.
// func SaveRecovery(dataAccessKey, Serial string, curTime time.Time) error {
// 	GlobalMtx.Lock()
// 	defer GlobalMtx.Unlock()

// 	// var recoveryElements []RecoveryElement

// 	recoveryElementMap.Range(func(key, value interface{}) bool {
// 		recoveryElements = append(recoveryElements, value.(RecoveryElement))
// 		return true
// 	})
// }

// 	recoveryJson, err := json.MarshalIndent(recoveryElements, "", "  ")

// 	if err != nil {
// 		return err
// 	}

// 	if string(recoveryJson) == "null" {
// 		ioutil.WriteFile(filePath, []byte{}, 0644)
// 	} else {
// 		ioutil.WriteFile(filePath, recoveryJson, 0644)
// 	}

// 	return nil
// }

// //loadRecovery 함수는 복구 관련 정보를 읽는 기능을 한다.
// func loadRecovery(filePath string, recoveryElementMap *sync.Map) error {
// 	GlobalMtx.Lock()
// 	defer GlobalMtx.Unlock()

// 	var recoveryElements []RecoveryElement

// 	_, err := os.Stat(filePath)
// 	if os.IsNotExist(err) {
// 		return nil
// 	}

// 	configFile, err := os.Open(filePath)
// 	defer configFile.Close()
// 	if err != nil {
// 		return err
// 	}
// 	jsonParser := json.NewDecoder(configFile)
// 	jsonParser.Decode(&recoveryElements)

// 	for _, recoveryElement := range recoveryElements {
// 		tempKey := fmt.Sprintf("%v-%v", recoveryElement.Host, recoveryElement.Port)
// 		tempElement := RecoveryElement{recoveryElement.Host, recoveryElement.Port, recoveryElement.StartTime, recoveryElement.FirstFailData}

// 		recoveryElementMap.Store(tempKey, tempElement)
// 	}

// 	return nil
// }

// //InitRecovry 함수는 복구 기능을 초기화하는 기능이다.
// func InitRecovry(filePath string, logger *zap.SugaredLogger) (*Recovery, error) {
// 	configPath := filePath

// 	recovery := &Recovery{}
// 	recovery.ConfigPath = configPath
// 	// recovery.DBConnect = dbObj
// 	recovery.NetworkFailure = &sync.Map{}
// 	recovery.Logger = logger
// 	recovery.Running = &sync.Map{}

// 	err := loadRecovery(configPath, recovery.NetworkFailure)

// 	if err != nil {
// 		return nil, errors.New("Can't read config file")
// 	}

// 	return recovery, nil
// }

// //ArrangeGarbage 함수는 복구 데이터 중 필요 없는 데이터를 정리하는 기능을 한다.
// //예를 들면 서버 정보를 잘 못 입력하여 일어난 장애에 대해서 정리하는 기능을 한다.
// func (r Recovery) ArrangeGarbage(servers []config.Server) error {
// 	deleteServers := []string{}

// 	r.NetworkFailure.Range(func(key, value interface{}) bool {
// 		found := false

// 		for _, server := range servers {
// 			tempKey := fmt.Sprintf("%v-%v", server.Ip, server.Port)

// 			if tempKey == key {
// 				found = true
// 				break
// 			}
// 		}

// 		if !found {
// 			deleteServers = append(deleteServers, key.(string))
// 		}

// 		return true
// 	})

// 	for _, deleteServer := range deleteServers {
// 		r.NetworkFailure.Delete(deleteServer)
// 	}

// 	return nil
// }

// //convertDBTypeToDataItemString 함수는 디비에 저장되어 있는 enum 값을 json에서 사용 할 string name key 값으로 변경하는 기능을 한다.
// //TODO : ds_rtu 데이터 아이템(일사량, 대기온도 등) 추가 필요
// func convertDBTypeToDataItemString(group bool, DBType int) string {
// 	if group {
// 		switch DBType {
// 		case 1:
// 			return "AVERAGE_PHASE_VOLTAGE"
// 		case 2:
// 			return "AVERAGE_LINE_VOLTAGE"
// 		case 3:
// 			return "AVERAGE_VOLTAGE_CREST_FACTOR"
// 		case 4:
// 			return "AVERAGE_PHASE_CURRENT"
// 		case 5:
// 			return "AVERAGE_CURRENT_CREST_FACTOR"
// 		case 6:
// 			return "SUM_ACTIVE_POWER"
// 		case 7:
// 			return "SUM_REACTIVE_POWER"
// 		case 8:
// 			return "POWER_FACTOR"
// 		case 9:
// 			return "AVERAGE_FREQUENCY"
// 		case 10:
// 			return "AVERAGE_THD_PHASE_VOLTAGE"
// 		case 11:
// 			return "AVERAGE_THD_PHASE_CURRENT"
// 		case 12:
// 			return "AVERAGE_TEMPERATURE"
// 		case 13:
// 			return "AVERAGE_VOLTAGE_HARMONICS"
// 		case 14:
// 			return "AVERAGE_CURRENT_HARMONICS"
// 		case 15:
// 			return "SUM_APPARENT_POWER"
// 		case 16:
// 			return "INVERTER_CONVERSION_EFFICIENCY"
// 		case 17:
// 			return "LINE_VOLTAGE_R_S"
// 		case 18:
// 			return "LINE_VOLTAGE_S_T"
// 		case 19:
// 			return "LINE_VOLTAGE_T_R"
// 		case 20:
// 			return "ACCUMULATE_POWER_CONSUMPTION"
// 		case 21:
// 			return "ELECTRIC_CHARGE"
// 		case 22:
// 			return "AVERAGE_DC_VOLTAGE"
// 		case 23:
// 			return "SUM_DC_CURRENT"
// 		case 24:
// 			return "SUM_DC_ELECTRIC_POWER"
// 		case 25:
// 			return "SUM_DC_AMPERE"
// 		case 26:
// 			return "ACCUMULATE_DC_POWER_CONSUMPTION"
// 		}
// 	} else {
// 		switch DBType {
// 		case 1:
// 			return "PHASE_VOLTAGE"
// 		case 2:
// 			return "LINE_VOLTAGE"
// 		case 3:
// 			return "VOLTAGE_CREST_FACTOR"
// 		case 4:
// 			return "PHASE_CURRENT"
// 		case 5:
// 			return "CURRENT_CREST_FACTOR"
// 		case 6:
// 			return "ACTIVE_POWER"
// 		case 7:
// 			return "REACTIVE_POWER"
// 		case 8:
// 			return "POWER_FACTOR"
// 		case 9:
// 			return "FREQUENCY"
// 		case 10:
// 			return "THD_PHASE_VOLTAGE"
// 		case 11:
// 			return "THD_PHASE_CURRENT"
// 		case 12:
// 			return "TEMPERATURE"
// 		case 13:
// 			return "VOLTAGE_HARMONICS"
// 		case 14:
// 			return "CURRENT_HARMONICS"
// 		case 15:
// 			return "APPARENT_POWER"
// 		case 16:
// 			return "DC_VOLTAGE"
// 		case 17:
// 			return "DC_CURRENT"
// 		case 18:
// 			return "DC_ELECTRIC_POWER"
// 		case 19:
// 			return "DC_AMPERE"
// 		}
// 	}

// 	return ""
// }

// //convertDBDataToSendData 함수는 DB로부터 읽은 group, module 데이터를 읽어 서버에 보낼 json으로 변환하는 기능을 수행한다.
// func convertDBDataToSendData(groups []GroupMap, groupDatas []db.GroupData, moduleDatas []db.ModuleData, dataType string) []models.GroupData {
// 	returnGroupDatas := []config.GroupData{}

// 	tempGroupDatas := make(map[int][]config.Data)
// 	tempModuleDatas := make(map[int][]config.Data)

// 	for _, groupData := range groupDatas {
// 		tempData := config.Data{}

// 		tempData.Name = convertDBTypeToDataItemString(true, groupData.Type)
// 		if dataType == "initial" {
// 			tempData.Value.SetValid(float64(groupData.InitialValue))
// 		} else if dataType == "last" {
// 			tempData.Value.SetValid(float64(groupData.LastValue))
// 		} else if dataType == "min" {
// 			tempData.Value.SetValid(float64(groupData.MinValue))
// 		} else if dataType == "max" {
// 			tempData.Value.SetValid(float64(groupData.MaxValue))
// 		} else if dataType == "avg" {
// 			tempData.Value.SetValid(float64(groupData.AvgValue))
// 		}

// 		tempGroupDatas[groupData.EquipmentID] = append(tempGroupDatas[groupData.EquipmentID], tempData)
// 	}

// 	for _, moduleData := range moduleDatas {
// 		tempData := models.Data{}

// 		tempData.Name = convertDBTypeToDataItemString(false, moduleData.Type)
// 		if dataType == "initial" {
// 			tempData.Value.SetValid(float64(moduleData.InitialValue))
// 		} else if dataType == "last" {
// 			tempData.Value.SetValid(float64(moduleData.LastValue))
// 		} else if dataType == "min" {
// 			tempData.Value.SetValid(float64(moduleData.MinValue))
// 		} else if dataType == "max" {
// 			tempData.Value.SetValid(float64(moduleData.MaxValue))
// 		} else if dataType == "avg" {
// 			tempData.Value.SetValid(float64(moduleData.AvgValue))
// 		}

// 		tempModuleDatas[moduleData.EquipmentID] = append(tempModuleDatas[moduleData.EquipmentID], tempData)
// 	}

// 	for _, group := range groups {
// 		tempGroupData := models.GroupData{}

// 		tempGroupData.Id = group.GroupID
// 		tempGroupData.Data = tempGroupDatas[group.GroupID]

// 		for _, moduleID := range group.ModuleID {
// 			if len(tempModuleDatas[moduleID]) < 1 {
// 				continue
// 			}

// 			tempModuleData := models.ModuleData{}

// 			tempModuleData.Id = moduleID
// 			tempModuleData.Data = tempModuleDatas[moduleID]

// 			tempGroupData.ModuleData = append(tempGroupData.ModuleData, tempModuleData)
// 		}

// 		if (len(tempGroupData.Data) < 1) && (len(tempGroupData.ModuleData) < 1) {
// 			continue
// 		}

// 		returnGroupDatas = append(returnGroupDatas, tempGroupData)
// 	}

// 	return returnGroupDatas
// }

// sendMergeData 함수는 json 데이터를 서버로 전송 하는 기능을 한다.
// func sendMergeData(recovery *Recovery, dataAccessKey string, method string, host string, port uint16, version string, path string, sendData *models.MergedData, maxTryCount int) error {
// 	var url string
// 	var client *http.Client
// 	var request *http.Request
// 	var err error
// 	var responseLastError error
// 	var responseLastWarnings []string

// 	b := new(bytes.Buffer)

// 	json.NewEncoder(b).Encode(sendData)

// 	recovery.Logger.Debugf("dataAccessKey(%v) method(%v) host(%v) port(%v) version(%v) path(%v)", dataAccessKey, method, host, port, version, path)

// 	if len(version) == 0 {
// 		if len(path) == 0 {
// 			url = fmt.Sprintf("%v://%v:%v/%v/", method, host, port, "metering")
// 		} else {
// 			url = fmt.Sprintf("%v://%v:%v/%v/%v/", method, host, port, path, "metering")
// 		}
// 	} else {
// 		if len(path) == 0 {
// 			url = fmt.Sprintf("%v://%v:%v/api/%v/metering-devices/data", method, host, port, version)
// 		} else {
// 			url = fmt.Sprintf("%v://%v:%v/%s/api/%v/metering-devices/data", method, host, port, path, version)
// 		}
// 	}

// 	if request, err = http.NewRequest("POST", url, bytes.NewReader([]byte(b.String()))); err != nil {
// 		return err
// 	}

// 	if dataAccessKey != "" {
// 		request.Header.Add("Data-Access-Key", dataAccessKey)
// 	}

// 	request.Header.Set("Content-Type", "application/json; charset=utf-8")

// 	for tryCount := 0; tryCount < maxTryCount; tryCount++ {

// 		if method == "http" {
// 			client = &http.Client{
// 				Timeout: time.Duration(20 * time.Second),
// 			}
// 		} else {
// 			tr := &http.Transport{
// 				//TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
// 			}

// 			client = &http.Client{
// 				Transport: tr,
// 				Timeout:   time.Duration(20 * time.Second),
// 			}
// 		}

// 		var response *http.Response
// 		recovery.Logger.Infof("[%s] sendMergeData Sending merged data time(%v) tryCount(%v)", url, sendData.Timestamp, tryCount+1)
// 		recovery.Logger.Debugf("sendMergeData [%s]", b.String())
// 		if response, err = client.Do(request); err != nil {
// 			responseLastError = err
// 			continue
// 		}

// 		defer response.Body.Close()

// 		body, _ := ioutil.ReadAll(response.Body)

// 		responseLastWarnings, responseLastError = utils.CheckResponseData(true, true, string(body))

// 		recovery.Logger.Infof("[%s] Received sendMergeData response code(%v) data(%s) responseWarnings(%v)", host, response.StatusCode, string(body), responseLastWarnings)

// 		if responseLastError == nil {
// 			break
// 		}
// 	}

// 	if responseLastError != nil {
// 		err = errors.New("Can't send data")
// 	}

// 	return err
// }

// //StartFailoverSender 함수는 main package에서 측정 데이터 전송시 호출한다.
// //서버 설정 중 복구하도록 설정되어 있으면 장애 발생시 복구모드로 들어가고 데이터 전송 성공시 과거 데이터를 순차적으로 보내는 기능도 수행한다.
// func (r Recovery) StartFailoverSender(receiver chan MergeDataWithTime, isRecovery bool, configMap map[string]models.ConfigurationInfo, dataAccessKey string, serial string, method string, host string, port uint16, version string, path string, protocolVersion string, recoveryDataType string) {

// 	key := fmt.Sprintf("%v-%v", host, port)

// 	recoveryMode := false
// 	recoveryStartTime := time.Unix(0, 0)
// 	groupMaps := []GroupMap{}

// 	//module이 어떤 group에 속하는지 판단
// 	if info, exists := r.NetworkFailure.Load(key); exists && isRecovery {
// 		r.Logger.Info("there are data that has not been delivered, start recovery mode ")
// 		recoveryMode = true
// 		recoveryStartTime = info.(RecoveryElement).StartTime.Truncate(time.Minute)

// 		for _, configElement := range configMap {
// 			for _, groupElement := range configElement.SensorInfo.Groups {
// 				groupMap := GroupMap{}

// 				groupMap.GroupID = groupElement.Id

// 				for _, moduleElement := range groupElement.Modules {
// 					groupMap.ModuleID = append(groupMap.ModuleID, moduleElement.Id)
// 				}

// 				groupMaps = append(groupMaps, groupMap)
// 			}
// 		}
// 	}

// 	recoveryQueue := []MergeDataWithTime{}
// 	firstFailData := config.MergedData{}

// 	for {
// 		select {
// 		case data := <-receiver: //측정 데이터 전송 코드
// 			recoveryQueue = append(recoveryQueue, data)

// 			//복구 모드시에 데이터 전송하지 않음. 다만, recoveryQueue에 데이터가 계속 append되므로 recoveryQueue에서 가장 최근 2분 데이터만 저장. 그 이전 데이터는 복구 모두에서 데이터를 전송해줌
// 			if recoveryMode {
// 				skippedIndex := 0
// 				for idx, data := range recoveryQueue {
// 					if time.Now().Truncate(time.Minute).Add(-time.Minute*2).UnixNano() > data.StartTime.Add(time.Second*15).UnixNano() {
// 						skippedIndex = idx
// 					} else {
// 						break
// 					}
// 				}

// 				tmpQueue := recoveryQueue
// 				recoveryQueue = append([]MergeDataWithTime{}, tmpQueue[skippedIndex:]...)

// 				continue
// 			}

// 			//측정 데이터를 서버에 전송. 실패시 복구 모드로 변경(recoveryMode = true)
// 			for _, data := range recoveryQueue {
// 				if err := sendMergeData(&r, dataAccessKey, method, host, port, version, path, &data.Data, 1); err != nil {
// 					r.Logger.Errorf("[%s]failed to send data to server", host)

// 					if !isRecovery {
// 						continue
// 					}
// 					//1.장애 발생시 장애 발생 시점을 서버별로 기록합니다.
// 					r.Logger.Infof("[%s]failed to transfer data, start recovery mode", host)
// 					recoveryMode = true
// 					firstFailData = data.Data
// 					r.NetworkFailure.Store(key, RecoveryElement{host, port, data.StartTime, firstFailData})

// 					saveRecovery(r.ConfigPath, r.NetworkFailure)

// 					recoveryStartTime = data.StartTime.Truncate(time.Minute)

// 					//TODO: 설정 변경시에 groupMap을 변경하도록 수정
// 					groupMaps = groupMaps[:0]
// 					for _, configElement := range configMap {
// 						for _, groupElement := range configElement.SensorInfo.Groups {
// 							groupMap := GroupMap{}

// 							groupMap.GroupID = groupElement.Id

// 							for _, moduleElement := range groupElement.Modules {
// 								groupMap.ModuleID = append(groupMap.ModuleID, moduleElement.Id)
// 							}

// 							groupMaps = append(groupMaps, groupMap)
// 						}
// 					}
// 					break
// 				}
// 			}

// 			recoveryQueue = recoveryQueue[:0]
// 		default: //복구 코드
// 			//2.복구를 진행합니다. recoveryQueue는 항상 최근 2분 데이터를 저장하고 있습니다. recoveryQueue[0]는 가장 오래된 데이터가 있습니다.
// 			if !recoveryMode || len(recoveryQueue) == 0 {
// 				time.Sleep(time.Millisecond * 500)
// 				continue
// 			}

// 			//복구는 recoveryQueue에 남아있는 가장 오래된 데이터의 시점까지 진행
// 			if recoveryStartTime.UnixNano() >= recoveryQueue[0].StartTime.Truncate(time.Minute).UnixNano() {
// 				r.Logger.Infof("[%s]end recovery mode", host)
// 				recoveryMode = false
// 				r.NetworkFailure.Delete(key)
// 				saveRecovery(r.ConfigPath, r.NetworkFailure)
// 				continue
// 			}

// 			//장기간의 데이터를 복구할 경우, 실패시 전체를 다시 복구하는 것은 비효율적이므로 1시간마다 복구 정보를 갱신
// 			//데이터 중복 전송이 일어날 수 있음.
// 			if recoveryStartTime.Minute() == 0 {
// 				r.NetworkFailure.Store(key, RecoveryElement{
// 					Host:          host,
// 					Port:          port,
// 					StartTime:     recoveryStartTime,
// 					FirstFailData: firstFailData,
// 				})
// 				saveRecovery(r.ConfigPath, r.NetworkFailure)
// 			}

// 			var groupDatas []db.GroupData
// 			var moduleDatas []db.ModuleData
// 			var dataInfo models.MergedData
// 			var err error

// 			//첫번째 실패 데이터가 있을 경우, 메모리에 저장된 첫번째 데이터를 최우선적으로 복구.
// 			//DB의 저장시간 타이밍 문제로 인해 최초 실패 1분 데이터의 누적 값이 줄어들 수 있는 문제 보정용
// 			if firstFailData.Timestamp != nil {
// 				if err = sendMergeData(&r, dataAccessKey, method, host, port, version, path, &firstFailData, 5); err == nil {
// 					firstFailData = models.MergedData{}
// 					recoveryStartTime = recoveryStartTime.Add(time.Second * 60)
// 					time.Sleep(time.Millisecond * 500)
// 					continue
// 				} else {
// 					r.Logger.Error("failed to send data to server")
// 					time.Sleep(time.Second * 20)
// 					continue
// 				}
// 			}

// 			//database에서 복구할 데이터를 가져옴
// 			//db index 사용을 위해서 start_date를 사용하며, 해당 저장시간은 1분 단위로 가정함. DB 저장 간격이 달라질 경우, 해당 시간을 변경 해야 함
// 			if groupDatas, err = db.GetAllGroupDataByStartDate(r.DBConnect, db.GroupDataTableName1Minute, recoveryStartTime.Add(time.Second*-60), 3); err != nil {
// 				r.Logger.Error("database is unavailable. skip data")
// 				recoveryStartTime = recoveryStartTime.Add(time.Second * 60)
// 				continue
// 			}

// 			if moduleDatas, err = db.GetAllModuleDataByStartDate(r.DBConnect, db.ModuleDataTableName1Minute, recoveryStartTime.Add(time.Second*-60), 3); err != nil {
// 				r.Logger.Error("database is unavailable")
// 				recoveryStartTime = recoveryStartTime.Add(time.Second * 60)
// 				continue
// 			}

// 			if (len(groupDatas) == 0) && (len(moduleDatas) == 0) {
// 				recoveryStartTime = recoveryStartTime.Add(time.Second * 60)
// 				continue
// 			}

// 			if len(protocolVersion) != 0 {
// 				dataInfo.ProtocolVersion = protocolVersion
// 			}

// 			if len(protocolVersion) == 0 {
// 				if len(version) == 0 {
// 					dataInfo.Timestamp = fmt.Sprintf("\\/Date(%d000)/\\", int64(recoveryStartTime.Unix()))
// 				} else {
// 					dataInfo.Timestamp = recoveryStartTime.Format(time.RFC3339Nano)
// 				}
// 			} else {
// 				dataInfo.Timestamp = recoveryStartTime.Format(time.RFC3339Nano)
// 			}
// 			dataInfo.MissingData = true
// 			dataInfo.Serial = serial
// 			dataInfo.GroupData = convertDBDataToSendData(groupMaps, groupDatas, moduleDatas, recoveryDataType)

// 			if err = sendMergeData(&r, dataAccessKey, method, host, port, version, path, &dataInfo, 5); err == nil {
// 				recoveryStartTime = recoveryStartTime.Add(time.Second * 60)
// 				time.Sleep(time.Millisecond * 500)
// 			} else {
// 				r.Logger.Errorf("[%s]failed to send recovery data to server", host)
// 				time.Sleep(time.Second * 20)
// 			}

// 		}
// 	}
// }
